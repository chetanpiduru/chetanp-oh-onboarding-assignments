FROM 813361731051.dkr.ecr.ap-south-1.amazonaws.com/dockerhub:openjdk-8

ARG app=chetanp-oh-onboarding-assignment
ARG version=0.0.1-SNAPSHOT
ARG port=8080

ENV app=$app

RUN mkdir -p /data/releases/$app/
RUN mkdir -p /logs/$app

ARG gid=2001
ARG uid=1001
RUN groupadd -g $gid -o $app
RUN useradd -m -u $uid -g $gid -o -s /bin/bash $app

RUN echo $app
COPY ./target/$app-$version.jar /data/releases/$app/
COPY ./entrypoint.sh /

RUN chown -R $app:$app /data/releases/$app/
RUN chown -R $app:$app /logs/$app
RUN chmod +x entrypoint.sh

USER $app
RUN echo $app

EXPOSE $port
ENTRYPOINT ["/entrypoint.sh"]