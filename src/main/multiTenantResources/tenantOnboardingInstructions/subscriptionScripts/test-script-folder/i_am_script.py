#!/usr/bin/env python
import requests

def trigger_test_api():
   response = requests.get(“https://www.example.com”)
   print("====================")
   if (response.status_code == 200):
       print("SUCCESS: Got success response")
       print(response.text)
   else:
       print("ERROR: Received failure")
       print(response.text)
   print("====================")

if __name__ == '__main__':
   trigger_test_api()